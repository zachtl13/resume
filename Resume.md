# Zachary Long

#### Contact
Phone: (804) 305-3047 | Email: zachtl13@vt.edu

#### Links
* [GitHub](https://masterfishy.github.io/)
* [GitLab](https://gitlab.com/zachtl13/)
* [LinkedIn](www.linkedin.com/in/zachlong-42 )

## Objective
To obtain an internship/part-time job for the Summer of 2019 in the field of CS.

## Education
**Virginia Tech**<br>
B.S. in Computer Science<br>
*Blacksburg, VA | Expected May 2021*<br>
**GPA**: 3.88 / 4.00

## Skills
* **Proficient in**: Python, Java, C#, MATLAB, Git, and Linux/Unix
* **Familiar with**: C++, HTML/CSS, JavaScript, and SQLite
* **APIs**: TensorFlow, Flask, Unity, Pygame
* Readable code
* Semi-fluent in Spanish
* Microsoft Office
* Windows 10

## Research/Work Experience
**Interactive Genome Analysis Lab** <br>
Lead Programmer and Data Analyst <br>
*Fall 2018 - Present | Blacksburg, VA*
* Analyze patterns in non-coding RNA sequence mutations looking for selection or rejection.
* Develop a database to gather and organize information.
* Assist in visualization of data.
<br> *Languages: Python, SQLite, Bash*

## Software Experience
**Global Game Jam** <br>
Level Developer and UI/UX Lead <br>
*January 26-28, 2018 | Blacksburg, VA*
* Collaborated in a team of 4 to develop a game in 48 hours based on a provided theme.
* Designed intuitive user interface and user experience.
* Developed increasingly difficult puzzle levels.
* Created a playable puzzle solving game using the Unity Game Engine.
<br> *Language: C#*

**Malamalama** <br>
Personal Project <br>
*May 2017 | Richmond, VA*
* Developed a game individually using Pygame.
* Taught spatial awareness and persistence through game mechanics.
* Learned readable coding habits and efficient problem solving.
<br> *Language: Python*

**National Student Leadership Council** <br>
UI/UX Lead <br>
*Summer 2016 | District of Columbia*
* Collaborated in a team of 4 to develop a game in 9 days using the Unity Game Engine.
* Managed team tasks to streamline the development process to ensure project completion by the deadline.
* Created a local two-player game.
<br> *Language: C#*

## Relevant Coursework
**Intro to Data Structures and Algorithms** <br>
Java <br>
*Fall 2018*

**Computer Organization** <br>
C <br>
*Spring 2019*

## Leadership
**Gaming Project at Virginia Tech** <br>
Vice President <br>
*Fall 2018 - Present | Blacksburg, VA*
* Run meetings about technical and theoretical concepts of game design.
